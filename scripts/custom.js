jQuery('#testimonials-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    arrows: false
  });

  jQuery(".wp_boder-btn").click(function() {
      var sec = jQuery("#contactform-wp");

      jQuery('html,body').animate({
          scrollTop: jQuery(sec).offset().top},
          'medium');
  });
  var newCustomSwiper = new Swiper ('.custom_swiper', {
    slidesPerView: 2,
      spaceBetween: 30,
      allowSlidePrev:true,
    loop: true,
    autoplay: {
        delay: 5000,
        disableOnInteraction: false,
      },
      // pagination: {
      //   el: '.swiper-pagination',
      //   type: 'bullets',
      // },
    // pagination: false,
    navigation: true,
    navigation: {
      nextEl: '.swiper_button_next',
      prevEl: '.swiper_button_prev',
    },
    breakpoints: {
      // when window width is >= 320px
      990: {
        slidesPerView: 1,
      },
    }
  });


$('.custom_swiper').on('mouseenter', function(e){
    newCustomSwiper.autoplay.stop();

})
$('.custom_swiper').on('mouseleave', function(e){
    newCustomSwiper.autoplay.start();
  })

  

  $('#client-laptop-slider').slick({
    dots: true,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    slidesToScroll: 1,
    variableWidth: true
    });

//  gsap animations 

    // gsap.timeline({
    //   scrollTrigger: {
    //     trigger: ".thumbnail_image",
    //     start: "0% 100%",
    //     end: "top 50%",
    //     scrub: true,
    //     pin: false
    //   }
    // })
    // .from(".thumbnail_image",  { x: 100 })

    // gsap.timeline({
    //   scrollTrigger: {
    //     trigger: ".col-lg-5 .slider",
    //     start: "0% 100%",
    //     end: "top 50%",
    //     scrub: true,
    //     pin: false
    //   }
    // })
    // .from(".col-lg-5 .slider",  { x: -100 })

    // gsap.timeline({
    //   scrollTrigger: {
    //     trigger: "section.slider",
    //     start: "0% 100%",
    //     end: "top 20% ",
    //     scrub: true,
    //     pin: false
    //   }
    // })
    // .from("section.slider",  { y: 240})

    // gsap.timeline({
    //   scrollTrigger: {
    //     trigger: ".full_width_blockquote",
    //     start: "0% 100%",
    //     end: "bottom 0% ",
    //     scrub: true,
    //     pin: false
    //   }
    // })
    // .from(".full_width_blockquote img",  { scale: 1.3}) 

    // gsap.timeline({
    //   scrollTrigger: {
    //     trigger: ".gallery_banner_top",
    //     start: "0% 20%",
    //     end: "bottom 0% ",
    //     scrub: true,
    //     pin: false
    //   }
    // })
    // .to(".gallery_banner_top .bankground_img img",  { scale: 1.3}) 

//  gsap animations end

function handle(delta) {
  var time = 1000;
  var distance = 300;

  $('html, body').stop().animate({
      scrollTop: $(window).scrollTop() - (distance * delta)
  }, time );
}
// class Scrooth {
//   constructor({element = window, strength=18, acceleration = 1.2,deceleration = .927}={}) {
//     this.element = element;
//     this.distance = strength;
//     this.acceleration = acceleration;
//     this.deceleration = deceleration;
//     this.running = false;
//     this.element.addEventListener('wheel', this.scrollHandler.bind(this), {passive: false});
//     this.element.addEventListener('mousewheel', this.scrollHandler.bind(this), {passive: false});
//     this.scroll = this.scroll.bind(this);
//   }
//   scrollHandler(e) {
//     e.preventDefault();
//     if (!this.running) {
//       this.top = this.element.pageYOffset || this.element.scrollTop || 0;
//       this.running = true;
//       this.currentDistance = e.deltaY > 0 ? 1.5 : -1.5;
//       this.isDistanceAsc = true;
//       this.scroll();
//     } else {
//       this.isDistanceAsc = false;
//       this.currentDistance = e.deltaY > 0 ? this.distance : -this.distance;
//     }
//   }
//   scroll() {
//     if (this.running) {
//       this.currentDistance *= this.isDistanceAsc === true ? this.acceleration : this.deceleration;
//       Math.abs(this.currentDistance) < 0.1 && this.isDistanceAsc === false ? this.running = false : 1;
//       Math.abs(this.currentDistance) >= Math.abs(this.distance) ? this.isDistanceAsc = false : 1;
//       this.top += this.currentDistance;
//       this.element.scrollTo(0, this.top);
//       requestAnimationFrame(this.scroll);
//     }
//   }
// }
// const body = new Scrooth();
// Scrollbar.init(document.querySelector('.scroller_container'));
// const locoScroll = new LocomotiveScroll({
//   el: document.querySelector(".scroller_container"),
//   smooth: true,
//   offset:['30px','50px'],
//   repeat:true,
//   lerp:0.07,
//   multiplier:.8,
// }); 
// locoScroll.on('scroll', (args) => {
//   // Get all current elements : args.currentElements
//   if(typeof args.currentElements['header'] === 'object') {
//       let progress = args.currentElements['header'].progress;
//       if(progress>=0.1){
//         jQuery('body').addClass('sticky_left')
//       }else{
//         jQuery('body').removeClass('sticky_left')
//       }
//       // ouput log example: 0.34
//       // gsap example : myGsapAnimation.progress(progress);
//   }
// });


var viewPort = jQuery(window).width();

function visibleElInViewPort(el) {
  var top = el.offsetTop;
  var left = el.offsetLeft;
  var width = el.offsetWidth;
  var height = el.offsetHeight;

  while (el.offsetParent) {
    el = el.offsetParent;
    top += el.offsetTop;
    left += el.offsetLeft;
  }

  return (
    top < window.pageYOffset + window.innerHeight &&
    left < window.pageXOffset + window.innerWidth &&
    top + height > window.pageYOffset &&
    left + width > window.pageXOffset
  );
}
var scrollEl = document.querySelector(".scroller_container");
var Scrollbar = window.Scrollbar;
var winHeight = scrollEl.clientHeight;
var bodyScroll;
var fired = false;



if (viewPort > 1280) {
  bodyScroll = Scrollbar.init(scrollEl, {
    damping: 0.1,
    overscrollEffect: "bounce",
    disableScroll: { direction: 'x' },
  });

  // jQuery('.burger').click(function(){
  //   bodyScroll.destroy();
  //   jQuery('body').addClass('overflow_desktop');
  // })
  // jQuery('.closeburger').click(function(){
  //   jQuery('body').removeClass('overflow_desktop');
  //   // jQuery('body').removeClass('overflow');
  //   jQuery('.navigation-menu').removeClass('active');
  //   jQuery('.navbar').removeClass('light');
  //   bodyScroll = Scrollbar.init(scrollEl, {
  //     damping: 0.1,
  //     overscrollEffect: "bounce",
  //     disableScroll: { direction: 'x' },
  //   });
  // })

  jQuery('.gotop').click(function(){
    // scrollToElem(jQuery('header'));
    bodyScroll.scrollIntoView(document.querySelector('header'), {
      offsetTop: 0,
      offsetLeft: 0,
      onlyScrollIfNeeded: true,
    });
  })

  bodyScroll.addListener(
    function (status) {
      var docHeight = scrollEl.scrollHeight;
      var scrollAmount = status.offset.y;
      var scrolltrigger = 0.95;
      var scrollPercent = (scrollAmount / (docHeight - winHeight)) * 100;

      // Scroll progress
      jQuery(".scroll-progress").width(scrollPercent + "%");

      // Activate sticky
      if (scrollPercent >= 6) {
        jQuery("body").addClass("sticky_header");
      } else {
        jQuery("body").removeClass("sticky_header");
      }

      // Check page end
      if (scrollAmount / (docHeight - winHeight) > scrolltrigger) {
        jQuery("body").addClass("scroll-end");
      } else {
        jQuery("body").removeClass("scroll-end");
      }

      // Banner Aniamtion
      var scale = (scrollPercent / 40) + 1;
      // $('.explore_our_works.visible .titles .custom-btn').css('transform', 'matrix(' + scale + ', 0, 0, ' + scale + ', '+scrollPercent+', '+scrollPercent+')');
      // $('.explore_our_works.visible .cards_grid_block .col-sm-12').css('transform', 'translateY(' + scrollPercent*(-10)+'px)');
      // $('.off_grid_column.visible .grid_item:first-child .thumbnail_image').each(function(){
      // $(this).css('transform', 'translateY(' + scale*(-200)+'px)');

      // })
      

      // Active sections
      var sections = jQuery("section");
      var sectionLength = jQuery(sections).length;
      for (var i = 0; sectionLength > i; i++) {
        if (bodyScroll.isVisible(sections[i])) {
          jQuery(sections[i]).addClass("visible");
          var elems = jQuery(sections[i]).find("[data-aos]");
          for (var j = 0; jQuery(elems).length > j; j++) {
            if (bodyScroll.isVisible(elems[j])) {
              jQuery(elems[j]).addClass("aos-init aos-animate");
            }
          }
        }else{
          jQuery(sections[i]).removeClass("visible");
          var elems = jQuery(sections[i]).find("[data-aos]");
          for (var j = 0; jQuery(elems).length > j; j++) {
            // if (bodyScroll.isVisible(elems[j])) {
              // jQuery(elems[j]).removeClass("aos-init aos-animate");
            // }
          }
        }
      }
    },
    {
      capture: true,
      passive: true,
    }
  );
}
var sections = jQuery("section");
var sectionLength = jQuery(sections).length;
for (var i = 0; sectionLength > i; i++) {
  if (visibleElInViewPort(sections[i])) {
    jQuery(sections[i]).addClass("visible");
    var elems = jQuery(sections[i]).find("[data-aos]");
    for (var j = 0; jQuery(elems).length > j; j++) {
      if (visibleElInViewPort(elems[j])) {
        jQuery(elems[j]).addClass("aos-init aos-animate");
      } else {
        jQuery(elems[j]).removeClass("aos-init aos-animate");
      }
    }
  } else {
    jQuery(sections[i]).removeClass("visible");
  }
}

// var cardsListSection = document.querySelector("section.cards-listing");
// if (cardsListSection) {
//   if (visibleElInViewPort(cardsListSection)) {
//     setTimeout(function () {
//       jQuery("section.cards-listing")
//         .find(".card.brand, .card.product, .card.news")
//         .each(function () {
//           jQuery(this).addClass("loaded");
//         });
//     }, 2000);
//   }
// }

var firstScroll = true;
var offsetTopVal = 100;
function scrollToElem(elem) {
  var offset = jQuery(elem).offset().top;

  if (firstScroll) {
    offsetTopVal = 385;
  } else {
    offsetTopVal = 135;
  }
  jQuery("[data-aos]").addClass("aos-init aos-animate");
  if (viewPort > 860) {
    bodyScroll.scrollIntoView(document.querySelector(elem), {
      offsetTop: offsetTopVal,
      offsetLeft: 0,
      onlyScrollIfNeeded: true,
    });
  } else {
    jQuery("html, body").animate(
      {
        scrollTop: offset,
      },
      300
    );
  }

  firstScroll = false;
}
if(window.location.hash != ''){
  var elem = '.'+window.location.hash.split('#')[1];
  console.log(elem);
  scrollToElem(elem);
}