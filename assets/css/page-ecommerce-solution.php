
<?php
/* Template Name: Ecommerce solution */?>
<?php get_header(); ?>
<div class="page-title">
  <div class="container">
    <h1><?php the_title(); //the_field('single_title'); ?></h1>
    <h2><?php the_field('single_description'); ?></h2>
  </div>
<!-- end container -->
</div>
</header>
<!-- end header -->

<?php
 $image=  get_field('banner_image');
 $title = $image['title'];
 $alt = $image['alt'];
 $thumb = $image['sizes'][ 'img-1400x760' ];

 $video =  get_field('banner_video'); 
 $banner_type = get_field('single_banner_type');
?>
<?php if($banner_type == 'image' && $image){?>
<section class="page-header">
  <figure>
    <picture>
      <source srcset="<?php echo webpUrl($thumb); ?>" type="image/webp">
      <source srcset="<?php echo $thumb; ?>" type="image/jpeg">
      <img src="<?php echo $thumb; ?>" alt="<?php echo $alt; ?>">
    </picture>
  </figure>
</section>

<?php } elseif($banner_type == 'video' && $video) { ?>
<section class="page-header">
  <div class="video-bg">
    <video src=" <?php echo $video['url']; ?>" loop autoplay muted></video>
  </div>
 </section>
<?php } ?>
<!-- end page-header -->

<?php
      $section2_title = get_field('section2_title');
      $section2_sub_title = get_field('section2_sub_title');
      $section2_description = get_field('section2_description');

      if( $section2_title || $section2_sub_title || $section2_description ) :
        echo '<section class="about-intro service-section-header pb-0">
        <div class="container">
        <div class="row"><div class="col-12 mb-2">';
          echo '<h3>'.$section2_title.'</h3>';
          echo '<h4>'.$section2_sub_title.'</h4>';
          echo '<p>'.$section2_description.'</p>';

        echo '</div></div></div></section>';
      endif;
    ?>



    <?php
      $section3_lists = get_field('section3_lists');
      $section3_contents = get_field('section3_contents');
      $getintouch_enabledisable = get_field('getintouch_enabledisable');

    ?>



    <!-- end col-12 -->

    <?php if($section3_lists) : ?>
      <section class="about-intro service-section pb-0">
        <div class="container">
        <div class="row">
        <div class="col-md-4 col-sm-4">
          <div class="well">
            <?php $custom3title = get_field("section3_title");
            if($custom3title == 1):?>
            <h5><?php the_field("custom_section3_title");?></h5>
            <?php else: ?>
            <h5>We specialise in:</h5>
            <?php endif;?>
            <?php echo $section3_lists; ?>
          </div>          
        </div>
        <div class="col-md-8 col-sm-8">
          <?php echo $section3_contents; ?>
          <!-- getin touch button -->
          <?php
            $getintouch_enabledisable = get_field('getintouch_enabledisable');
            if( $getintouch_enabledisable == 'true' ){ ?>
              <?php
                $get_in_touch_link = get_field('get_in_touch_link','options');
                if($get_in_touch_link) :
                  $link_url = $get_in_touch_link['url'];
                  $link_title = $get_in_touch_link['title'];
                  $link_target = $get_in_touch_link['target'] ? $get_in_touch_link['target'] : '_self';
                  echo '<div class="custom-btn dark get-in-touch-service"><a href="'.$link_url.'" target="'.$link_target.'">'.$link_title.' <span></span> <i></i> </a></div>';
                endif;
              ?> 
          <?php } else{ ?>
          <?php }  ?>
          <!-- getin touch button end -->
        </div>
        </div>
        </div>
      </section>
    <?php else : ?>
      <?php
                  if( $getintouch_enabledisable == 'true' ): ?>
              <section class="about-intro service-section pb-0">
                  <div class="container">
                  <div class="row">
                      <div class="col-md-12 col-sm-12">
                        <?php echo $section3_contents; ?>
                        <!-- getin touch button -->
                      
                            <?php
                              $get_in_touch_link = get_field('get_in_touch_link','options');
                              if($get_in_touch_link) :
                                $link_url = $get_in_touch_link['url'];
                                $link_title = $get_in_touch_link['title'];
                                $link_target = $get_in_touch_link['target'] ? $get_in_touch_link['target'] : '_self';
                                echo '<div class="custom-btn dark get-in-touch-service"><a href="'.$link_url.'" target="'.$link_target.'">'.$link_title.' <span></span> <i></i> </a></div>';
                              endif;
                            ?> 
                        <!-- getin touch button end -->
                      </div>
                    </div>
                  </div>
                </section>
        <?php endif;  ?>
    <?php endif; ?>



<?php 
$posts = get_field('related_projects');
if($posts):
  $count = count($posts);
?>
<section class="related-projects">
  <div class="container">
    <div class="row">
      <div class="col-md-12 title recent">
          <h2>Recent Project<?php echo ($count > 1) ? 's' : ''; ?> </h2>
      </div>
      <?php 
      if( $posts ): ?>
          <?php foreach( $posts as $post): ?>
              <?php setup_postdata($post); 
                $img_url = get_the_post_thumbnail_url(get_the_ID(),'img_555x517');
              ?>
                <div class="col-md-4 col-sm-4 col-xs-12 related-items">
                  <a href="<?php the_permalink(); ?>">
                    <picture>
                      <source srcset="<?php echo webpUrl($img_url); ?>" type="image/webp">
                      <source srcset="<?php echo $img_url; ?>" type="image/jpeg">
                      <img src="<?php echo $img_url; ?>" alt="<?php the_title(); ?>">
                    </picture>
                  </a><br/><br/>
                  <div class="fig-name title"><a href="<?php the_permalink(); ?>"><h5><?php the_title(); ?></h5></a> </div>
                </div>
          <?php endforeach; ?>
          <?php wp_reset_postdata(); ?>
      <?php endif; ?>
    </div>
  </div>
</section>
<?php endif; ?>


<?php

// Check value exists.
if( have_rows('details') ):

    // Loop through rows.
    while ( have_rows('details') ) : the_row();
      if( get_row_layout() == 'image' ):
        $image_bg_img = get_sub_field('image_bg');
        $image_bg = $image_bg_img['url'];
    
      echo'<section class="about-intro service-section pb-0">
        <div class="container">
          <div class="row">
              <img src="'.$image_bg.'" alt="">
          </div>
        </div>
      </section>';

      elseif( get_row_layout() == 'description' ): 
            $description_bt = get_sub_field('description_bt');
      echo'<section class="about-intro service-section pb-0">
        <div class="container">
          <div class="row">
            <div class="col">
                '.$description_bt.'
            </div>
          </div>
        </div>
      </section>';
    endif;
  endwhile;
endif;
  ?>




<?php get_footer(); ?>