/**
 *  Copyright 2017 Amardeep Rai
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

'use strict';

var autoPrefixBrowserList = ['last 2 version', 'safari 5', 'ie 8', 'ie 9', 'opera 12.1', 'ios 6', 'android 4'];
var autoprefixer = require('gulp-autoprefixer');
var csso = require('gulp-csso');
var del = require('del');
var gulp = require('gulp');
var htmlmin = require('gulp-htmlmin');
var runSequence = require('run-sequence');
var sass = require('gulp-sass');
var gutil = require('gulp-util');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var plumber = require('gulp-plumber');
var gulpImports = require('gulp-imports');
var minifyCSS = require('gulp-minify-css');

// Gulp task to minify CSS files
gulp.task('styles', function () {
  return gulp.src('./styles/src/init.scss')
    //include SCSS and list every "include" folder
    .pipe(sass({
        errLogToConsole: true,
        includePaths: [
            'styles/src'
        ]
    }))
    .pipe(autoprefixer({
      browsers: autoPrefixBrowserList,
      cascade: true
    }))
    //catch errors
    .on('error', gutil.log)
    //the final filename of our combined css file
    .pipe(concat('styles.min.css'))
    .pipe(minifyCSS())
    //where to save our final, compressed css file
    .pipe(gulp.dest('styles/'))
});

// Gulp task to minify JavaScript files
gulp.task('jquery', function() {
  //this is where our dev JS scripts are
  return gulp.src(['scripts/src/_vendor/jquery.min.js'])
      //prevent pipe breaking caused by errors from gulp plugins
      .pipe(plumber())
      //this is the filename of the compressed version of our JS
      .pipe(concat('jquery.min.js'))
      //compress :D
      .pipe(uglify())
      //where we will store our finalized, compressed script
      .pipe(gulp.dest('scripts/'));
});

gulp.task('plugins', function() {

  return gulp.src(['./scripts/src/plugins.js'])

    //cloning all js together   
    //.pipe(jsImport({hideConsole: true}))
    .pipe(gulpImports())
    //prevent pipe breaking caused by errors from gulp plugins
    .pipe(plumber())
    //this is the filename of the compressed version of our JS
    .pipe(concat('plugins.min.js'))
    //catch errors
    .on('error', gutil.log)
    //compress :D
    .pipe(uglify())
    //where we will store our finalized, compressed script
    .pipe(gulp.dest('scripts/'))
});

// Gulp task to minify JavaScript files
gulp.task('scripts', function() {

  return gulp.src(['./scripts/src/scripts.js'])

    //cloning all js together   
    //.pipe(jsImport({hideConsole: true}))
    .pipe(gulpImports())
    //prevent pipe breaking caused by errors from gulp plugins
    .pipe(plumber())
    //this is the filename of the compressed version of our JS
    .pipe(concat('scripts.min.js'))
    //catch errors
    .on('error', gutil.log)
    //compress :D
    .pipe(uglify())
    //where we will store our finalized, compressed script
    .pipe(gulp.dest('scripts/'));
});

// Gulp task to minify all files
gulp.task('default', ['styles', 'scripts', 'plugins'] );